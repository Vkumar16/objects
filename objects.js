function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    //return Object.values(obj)
    let valuesArray = [];
    if (obj) {
        for (let key in obj) {
            valuesArray.push(obj[key])
        }
    }
    return valuesArray;
}
module.exports = values