function mapObject(obj, cb) {
    // Like map for arrays, but for objects. 
    //Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    let newObject = {};
    if(obj)
    {
        for(let key in obj)
        {
            newObject[key]  = cb(obj[key],key) //cb(value,key)
        }
    }
    return newObject;
}
module.exports = mapObject;