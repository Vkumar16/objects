function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    //return Object.keys(obj);
    let keysArray = []
    for(let key in obj)
    {
        keysArray.push(key);
    }
    return keysArray;
}
module.exports = keys
