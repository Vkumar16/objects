function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let pairsArray = [];
    if(obj)
    {
        for(let key in obj)
        {
            let arr = [];
            arr.push(key);
            arr.push(obj[key]);
            pairsArray.push(arr);
        }
    }
    return pairsArray;
}

module.exports = pairs;